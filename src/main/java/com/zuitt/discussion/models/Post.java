package com.zuitt.discussion.models;

import javax.persistence.*;

//Marks this objectg as a representation of an entity/record from the database table "posts"
@Entity
//Designate the table name re;ated tp the model
@Table(name = "posts")
public class Post {

    //properties
    //indicates primary keyu
    @Id
    //id will be autoincremented
    @GeneratedValue
    private long id;

    //class properties that represents table column in a relational database are annotated as @Column
    @Column
    private String title;

    @Column
    private String content;

    //constructors
    //default constructor are required when retrieving data from database
    public Post(){}

    public Post(long id, String title,String content){
        this.title = title;
        this.content = content;
    }

    //Getters and Setters
    public  String getTitle(){
        return title;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public String getContent(){
        return content;
    }
    public void setContent(String content){
        this.content = content;
    }
}
