package com.zuitt.discussion.repositories;


import com.zuitt.discussion.models.Post;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


//Post is teh data type of the data used in the methods
// Projects is the data returned from the database
//an interface marked as @Repository contains method for databse manipulation
@Repository
public interface PostRepository extends CrudRepository<Post,Object> {

}
